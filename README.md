# README #

**The Problem**

Find the best truck to deliver a cargo, minimizing the overall distances all trucks must travel.

**The Solution**

This algorithm uses the Googlemaps API or the Geopy library to calculate the distance between some origin and a set of destinations. The idea is to find the best truck to deliver a cargo. 

The heuristic being used is: the truck nearest to the cargo origin will be elected to transport the cargo. As each truck can only carry up to one cargo and each truck can only make up to one trip, this heuristic solves the problem with simplicity.

**Install**

```
#!bash

python setup.py install
```

**Usage**

This script has two providers to calculate the best distance:

* Googlemaps: The googlemaps provider calculates the best distance using the Distance API and is accurate. To use this provider you will need a valid Distance API key. The usage is:

```
#!bash
landsmart -c cargo.csv -t truck.csv -p googlemaps -k <my_api_key>
```

* Geopy: The geopy provider calculates the best distance using the vincenty formula (https://en.wikipedia.org/wiki/Vincenty's_formulae). This provider may be not reliable, because the routes are calculated not considering roads, but this provider works offline and is fast.

```
#!bash
landsmart -c cargo.csv -t truck.csv -p geopy
```

To access the help:

```
#!bash

landsmart -h

usage: loadsmart [-h] [-t TRUCKS_FILENAME] [-c CARGO_FILENAME] [-p PROVIDER]
                 [-k API_KEY]

Finds the best allocation of trucks to cargo, using the best route, i.e, the
route with the smaller distance.

optional arguments:
  -h, --help            show this help message and exit

Files:
  -t TRUCKS_FILENAME, --trucks-filename TRUCKS_FILENAME
                        Trucks Filename
  -c CARGO_FILENAME, --cargo-filename CARGO_FILENAME
                        Cargo Filename

Provider:
  -p PROVIDER, --provider PROVIDER
  -k API_KEY, --api-key API_KEY

```