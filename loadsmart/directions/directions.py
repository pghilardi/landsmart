


class Directions(object):
    """
    Simple interface to get the best distance from origin to a set of
    destinations.

    You may add different implementations for this API. Examples:
    - GoogleMaps
    - MapQuest
    - OpenStreetMap
    - GeoPy
    """

    # TODO: Maybe the API key can be obtained from a environment variable
    # in the future, instead being passed as a script parameter

    def setUp(self, api_key):
        """
        :param str api_key:
            The API key (optional). Some providers will not use this.
        """

    def get_best_distance(self, origin, destinations):
        """
        :param tuple(float, flot) origin:
            The origin to calculate the distance matrix. The origin must be a
            tuple of latitude and longitude coordinates.

        :param list(tuple(float,float)) destinations:
            The destinations to calculate the distance matrix. These origins
            must be latitude and longitude coordinates.

        :returns int, float:
            The index of the destinations which is the best and the distance
            in meters from origin to this destination.
        """
