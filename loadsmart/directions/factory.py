from loadsmart.directions.googlemaps import GoogleMaps
from loadsmart.directions.geopy import Geopy


class DirectionsFactory(object):
    """
    Build the proper direction client for the choosen provider.
    """

    DIRECTION_PROVIDER_TO_DIRECTIONS_CLIENT = {
        'googlemaps' : GoogleMaps,
        'geopy' : Geopy
    }

    def create(self, provider, api_key):
        """
        :param str provider:
            The desired provider.

        :param str api_key:
            The API key.

        :returns Directions:
            The directions client.
        """
        if provider not in self.DIRECTION_PROVIDER_TO_DIRECTIONS_CLIENT:
            raise KeyError('The provider %s is not supported.' % provider)

        directions = self.DIRECTION_PROVIDER_TO_DIRECTIONS_CLIENT[provider]()
        directions.setUp(api_key)
        return directions
