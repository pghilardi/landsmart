from geopy.distance import vincenty
from loadsmart.directions.directions import Directions


class Geopy(Directions):
    """
    This is an offline distance calculator that uses the vincenty formula:
    https://en.wikipedia.org/wiki/Vincenty's_formulae
    """

    def get_best_distance(self, origin, destinations):

        if not origin:
            raise RuntimeError('You must pass at least one origin')

        if not destinations:
            raise RuntimeError('You must pass at least one destination')

        best_distance = None
        best_index = None
        for index, destination in enumerate(destinations):
            distance = vincenty(origin, destination).meters
            if best_distance is None or distance < best_distance:
                best_distance = round(distance, 2)
                best_index = index

        return best_index, best_distance
