import googlemaps
from loadsmart.directions.directions import Directions

# Example key: 'AIzaSyC24Fd83Z9OZO-iMrP3q8VAP4dcgkEykn4'

class GoogleMaps(Directions):

    def setUp(self, api_key):
        self._client = googlemaps.Client(key=api_key, retry_timeout=5)

    def get_best_distance(self, origin, destinations):
        if not origin:
            raise RuntimeError('You must pass at least one origin')

        if not destinations:
            raise RuntimeError('You must pass at least one destination')

        try:
            matrix = self._client.distance_matrix([origin], destinations)
        except googlemaps.exceptions.Timeout:
            print('Timeout while getting info from distances API. Probably you exceeded the usage quota.')
            return None, None

        best_distance = None
        best_index = None
        if matrix:
            rows = matrix['rows'][0]
            elements = rows['elements']
            for index, element in enumerate(elements):
                if best_distance is None or element['distance']['value'] < best_distance:
                    if 'distance' in element:
                        best_distance = element['distance']['value']
                        best_index = index

        return best_index, best_distance
