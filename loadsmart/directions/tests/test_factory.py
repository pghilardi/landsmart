from loadsmart.directions.factory import DirectionsFactory
from loadsmart.directions.geopy import Geopy
import pytest


def test_create():
    factory = DirectionsFactory()
    directions = factory.create('geopy', api_key=None)
    assert type(directions) == Geopy

    with pytest.raises(KeyError) as e:
        factory.create('invalid', api_key=None)
