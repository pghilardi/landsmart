from loadsmart.directions.geopy import Geopy


def test_get_best_distance():
    geopy = Geopy()

    origin = (27.5954, 48.5480) # Florianopolis
    destinations = [
        (27.5954, 48.5480), # Florianopolis
        (27.0961, 48.6178), # Itapema
        (27.4956, 48.6554), # Biguacu
        (27.6141, 48.6371), # Sao Jose
        (27.9950, 48.7593), # Paulo Lopes
    ]
    best_index, best_distance = geopy.get_best_distance(origin, destinations)

    assert best_index == 0
    assert best_distance == 0.0

    origin = (27.5954, 48.5480) # Florianopolis
    destinations = [
        (27.0961, 48.6178), # Itapema
        (27.4956, 48.6554), # Biguacu
        (27.6141, 48.6371), # Sao Jose
        (27.9950, 48.7593), # Paulo Lopes
    ]
    best_index, best_distance = geopy.get_best_distance(origin, destinations)

    assert best_index == 2
    assert round(best_distance, 2) == 9036.61
