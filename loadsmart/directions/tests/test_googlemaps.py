from loadsmart.directions.googlemaps import GoogleMaps
from unittest.mock import patch
import googlemaps


class MockedGoogleMapsClient(object):

    def distance_matrix(self, origins, destinations):
        return {
            'rows' : [{
                'elements' : [{
                    'distance' : {
                        'value' : 3
                    }},{
                    'distance' : {
                        'value' : 10
                    }},{
                    'distance' : {
                        'value' : 22
                    }},{
                    'distance' : {
                        'value' : 11
                    }}
                ]
            }]
        }


def test_get_best_distance():
    # Just testing that the GoogleMaps implementation (my implementation)
    # always returns the smaller distance from the distances returned from
    # the maps API. I am considering that the Maps API is tested 
    maps = GoogleMaps()

    mocked_client = MockedGoogleMapsClient()
    with patch.object(googlemaps, 'Client', return_value=mocked_client) as mocked_client:
        maps.setUp('fake')
        origin = (10, 20)
        destinations = [(10, 23), (22, 21), (20, 50), (22, 30)]
        best_index, best_distance = maps.get_best_distance(origin, destinations)

        assert best_index == 0
        assert best_distance == 3.0
