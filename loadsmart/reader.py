import csv
import os


class InputReader(object):
    """
    Reads trucks and cargo information from .CSV files.
    """

    def read(self, trucks_filename, cargos_filename):
        """
        :param str trucks_filename:
            A valid filename for the trucks information.

        :param str cargos_filename:
            A valid filename for the cargos information.
        """

        def parse(filename, fieldnames, values):
            with open(filename) as f:
                reader = csv.DictReader(f, delimiter=',')

                fieldnames = set(fieldnames)
                reader_fieldnames = set(reader.fieldnames)
                diff = fieldnames - reader_fieldnames
                if len(fieldnames.difference(reader_fieldnames)) > 0:
                    raise RuntimeError('Error parsing input file: %s expecting keys: %s' % (filename, diff))

                for row in reader:
                    values.append(row)

        trucks, cargos = [], []

        fieldnames = ['truck','city','state','lat','lng']
        parse(trucks_filename, fieldnames, trucks)
        fieldnames = ['product','origin_city','origin_state','origin_lat',
            'origin_lng','destination_city','destination_state',
            'destination_lat','destination_lng']
        parse(cargos_filename, fieldnames, cargos)

        return trucks, cargos
