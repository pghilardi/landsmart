from loadsmart.reader import InputReader
from loadsmart.directions.googlemaps import GoogleMaps
from loadsmart.directions.factory import DirectionsFactory


class Solver(object):

    def __init__(self, provider, api_key=None):
        """
        Solve the best allocation of cargo to trucks using the provider.

        :param str provider:
            The provider type. Ex: googlemaps, geopy, mapquest, etc

        :param str api_key:
            The API key (optional)
        """
        self._available_trucks = []

        factory = DirectionsFactory()
        self._client = factory.create(provider, api_key)

    def solve(self, trucks_filename, cargos_filename):
        reader = InputReader();
        self._available_trucks, cargos = reader.read(trucks_filename, cargos_filename)

        print('Output: \n')
        for cargo in cargos:
            truck = self.get_nearest_truck_for_cargo(cargo)
            message = 'The cargo from %s to %s will be delivered by the truck %s from %s'
            if truck:
                print(message % (cargo['origin_city'], cargo['destination_city'], truck['truck'], truck['city']))

    def get_nearest_truck_for_cargo(self, cargo):
        origin = (cargo['origin_lat'], cargo['origin_lng'])
        destinations = [(truck['lat'], truck['lng']) for truck in self._available_trucks]
        best_distance_index, best_distance = self._client.get_best_distance(origin, destinations)

        if best_distance:
            print('Best distance: %.2f km' % (best_distance / 1000))

        truck = None
        if best_distance_index:
            truck = self._available_trucks.pop(best_distance_index)

        return truck
