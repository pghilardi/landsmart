import csv
import tempfile
import sys
import pytest
from io import StringIO
from contextlib import contextmanager
from loadsmart.reader import InputReader

def test_error_parsing_input():
    reader = InputReader()

    with tempfile.NamedTemporaryFile(mode='w', delete=False) as trucks:
        writer = csv.writer(trucks, delimiter=',')
        fieldnames = ['truck','city','state','invalid','lng']
        writer.writerow(fieldnames)

    with tempfile.NamedTemporaryFile(mode='w', delete=False) as cargo:
        writer = csv.writer(cargo, delimiter=',')
        fieldnames = ['product','origin_city','origin_state','origin_lat',
            'origin_lng','destination_city','destination_state',
            'destination_lat','destination_lng']
        writer.writerow(fieldnames)
        writer.writerow(['Espumante','Garibaldi','RS',29.2594,51.5254,'Florianopolis','SC',27.5954,48.5480])

    with pytest.raises(RuntimeError) as e:
        reader.read(trucks.name, cargo.name)

def test_parse_valid_input():
    reader = InputReader()

    with tempfile.NamedTemporaryFile(mode='w', delete=False) as trucks:
        writer = csv.writer(trucks, delimiter=',')
        fieldnames = ['truck','city','state','lat','lng']
        writer.writerow(fieldnames)
        writer.writerow(['Luis','Bento Goncalves','RS',29.1667,51.5170])

    with tempfile.NamedTemporaryFile(mode='w', delete=False) as cargo:
        writer = csv.writer(cargo, delimiter=',')
        fieldnames = ['product','origin_city','origin_state','origin_lat',
            'origin_lng','destination_city','destination_state',
            'destination_lat','destination_lng']
        writer.writerow(fieldnames)
        writer.writerow(['Espumante','Garibaldi','RS',29.2594,51.5254,'Florianopolis','SC',27.5954,48.5480])

    trucks, cargo = reader.read(trucks.name, cargo.name)
    assert len(trucks) == 1
    assert len(cargo) == 1
