import csv
import tempfile
import sys
from io import StringIO
from contextlib import contextmanager
from loadsmart.solver import Solver

@contextmanager
def capture(func, *args, **kwargs):
    """
    Executes a function, redirecting the stdout to the captured output.
    """
    out, sys.stdout = sys.stdout, StringIO()
    func(*args, **kwargs)
    sys.stdout.seek(0)
    yield sys.stdout.read()
    sys.stdout = out

def test_solver_using_geopy():
    solver = Solver('geopy')

    with tempfile.NamedTemporaryFile(mode='w', delete=False) as trucks:
        writer = csv.writer(trucks, delimiter=',')
        fieldnames = ['truck','city','state','lat','lng']
        writer.writerow(fieldnames)
        writer.writerow(['Luis','Bento Goncalves','RS',29.1667,51.5170])
        writer.writerow(['Jose','Farroupilha','RS',29.2232,51.3424])

    with tempfile.NamedTemporaryFile(mode='w', delete=False) as cargo:
        writer = csv.writer(cargo, delimiter=',')
        fieldnames = ['product','origin_city','origin_state','origin_lat',
            'origin_lng','destination_city','destination_state',
            'destination_lat','destination_lng']
        writer.writerow(fieldnames)
        writer.writerow(['Espumante','Garibaldi','RS',29.2594,51.5254,'Florianopolis','SC',27.5954,48.5480])

    solver = Solver('geopy')
    with capture(solver.solve, trucks.name, cargo.name) as output:
        splitted = output.split('\n')

    assert splitted[0] == 'Output: '
    assert splitted[1] == ''
    assert splitted[2] == 'Best distance: 10307.22'
    assert splitted[3] == 'The cargo from Garibaldi to Florianopolis will be delivered by the truck Luis from Bento Goncalves'
    assert splitted[4] == ''
