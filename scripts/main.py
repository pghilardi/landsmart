import sys
import argparse
import os
from loadsmart.directions.factory import DirectionsFactory

VALID_PROVIDERS = DirectionsFactory.DIRECTION_PROVIDER_TO_DIRECTIONS_CLIENT.keys()

class LoadsmartArgumentParser(argparse.ArgumentParser):
    """
    The argument parser for this script.
    """

    def __init__(self):
        description = """Finds the best allocation of trucks to cargo, using the
        best route, i.e, the route with the smaller distance.
        """
        argparse.ArgumentParser.__init__(self, description=description)

        files = self.add_argument_group('Files')
        files.add_argument(
            '-t', '--trucks-filename', type=str, help='Trucks Filename')
        files.add_argument(
            '-c', '--cargo-filename', type=str, help='Cargo Filename')

        provider = self.add_argument_group('Provider')
        provider.add_argument('-p', '--provider', type=str, default='googlemaps')
        provider.add_argument('-k', '--api-key', type=str)


def main():
    # Hold all found errors and then present them to the user.
    errors = []

    arg_parser = LoadsmartArgumentParser()
    args = arg_parser.parse_args()

    if args.trucks_filename is None or not os.path.isfile(args.trucks_filename):
        errors.append('You must pass a valid trucks filename')

    if args.cargo_filename is None or not os.path.isfile(args.cargo_filename):
        errors.append('You must pass a valid cargo filename')

    if args.provider not in VALID_PROVIDERS:
        errors.append('The provider %s is not a valid provider.' % args.provider)

    if args.provider == 'googlemaps' and not args.api_key:
        errors.append('The google maps provider needs a valid API key.')

    if errors:
        print('\nErrors found:')
        for error in errors:
            print('* ' + error)
        print('\n')
        return

    from loadsmart.solver import Solver
    solver = Solver(args.provider, api_key=args.api_key)
    solver.solve(args.trucks_filename, args.cargo_filename)
