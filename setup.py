"""A setuptools based setup module."""
from setuptools import setup, find_packages

setup(
    name='loadsmart',
    version='1.0.0',
    description='Find optimal allocation of trucks to cargos.',
    url='https://bitbucket.org/pghilardi/loadsmart/',
    author='Pedro Ghilardi',
    author_email='pghilardi@gmail.com',
    packages=find_packages(),
    entry_points = {
          'console_scripts': [
              'loadsmart=scripts.main:main',
          ],
      },
)
